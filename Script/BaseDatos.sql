/*
drop table portaxtr.tbl_movimiento;
drop table portaxtr.tbl_cuenta;
drop table portaxtr.tbl_cliente;
drop table portaxtr.tbl_persona;


select a.*,rowid from portaxtr.tbl_persona a;
select a.*,rowid from portaxtr.tbl_cliente a;
select a.*,rowid from portaxtr.tbl_cuenta a;
select a.*,rowid from portaxtr.tbl_movimiento a;

  select
        sq_tbl_cliente_id_db.nextval 
    from
        dual
        
*/


create sequence sq_tbl_persona_id_db
minvalue 1
maxvalue 999999
start with 1
increment by 1
cache 20
order;

create sequence sq_tbl_cliente_id_db
minvalue 1
maxvalue 999999
start with 1
increment by 1
cache 20
order;

create sequence sq_tbl_cuenta_id_db
minvalue 1
maxvalue 999999
start with 1
increment by 1
cache 20
order;

create sequence sq_tbl_movimiento_id_db
minvalue 1
maxvalue 999999
start with 1
increment by 1
cache 20
order;

-- Create tbl_cliente
create table tbl_persona
(
  persona_id                    NUMBER NOT NULL,
  nombre                        VARCHAR2(200),
  genero                        VARCHAR2(100),
  edad                          number,
  identificacion                VARCHAR2(20),
  direccion                     VARCHAR2(200),
  telefono                      VARCHAR2(20),
  usuario_registro               varchar2(50) default user,
  fecha_registro                 DATE default sysdate
);
 
ALTER TABLE tbl_persona
ADD CONSTRAINT tbl_persona_id_pk PRIMARY KEY (persona_id);

-- Create tbl_cliente
create table tbl_cliente
(
  cliente_id                    NUMBER NOT NULL,
  persona_id                    NUMBER NOT NULL,
  contrasenia                   VARCHAR2(20),
  estado                        varchar(10),
  usuario_registro               varchar2(50) default user,
  fecha_registro                 DATE default sysdate
);
 
ALTER TABLE tbl_cliente
ADD CONSTRAINT tbl_cliente_id_pk PRIMARY KEY (cliente_id);

ALTER TABLE tbl_cliente
ADD CONSTRAINT tbl_persona_clienteid_fk
   FOREIGN KEY (persona_id)
   REFERENCES tbl_persona (persona_id);
-- Create tbl_cuenta
create table tbl_cuenta  
(
  cuenta_id                      NUMBER NOT NULL,
  cliente_id                     NUMBER NOT NULL,
  numero_cuenta                  VARCHAR2(50),
  tipo_cuenta                    VARCHAR2(50),
  saldo_inicial                  number(15,2),
  estado                         varchar(10),
  usuario_registro               VARCHAR2(50) DEFAULT USER,
  fecha_registro                 DATE DEFAULT SYSDATE
)
tablespace CRM_DAT
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );

ALTER TABLE tbl_cuenta
ADD CONSTRAINT tbl_cuenta_id_PK PRIMARY KEY (cuenta_id);

ALTER TABLE tbl_cuenta
ADD CONSTRAINT tbl_cuenta_clienteid_fk
   FOREIGN KEY (cliente_id)
   REFERENCES tbl_cliente (cliente_id);


-- Create tbl_cuenta
create table tbl_movimiento  
(
  
  movimiento_id     NUMBER NOT NULL,
  cuenta_id         NUMBER NOT NULL,
  fecha             date,
  tipo_movimiento   VARCHAR2(50),
  valor             number(15,2),
  saldo             number(15,2),�
  usuario_registro  VARCHAR2(50) DEFAULT USER,
  fecha_registro    DATE DEFAULT SYSDATE
);

ALTER TABLE tbl_movimiento
ADD CONSTRAINT tbl_movimiento_id_PK PRIMARY KEY (movimiento_id);

ALTER TABLE tbl_movimiento
ADD CONSTRAINT tbl_movimiento_cuentaid_fk
   FOREIGN KEY (cuenta_id)
   REFERENCES tbl_cuenta (cuenta_id);
