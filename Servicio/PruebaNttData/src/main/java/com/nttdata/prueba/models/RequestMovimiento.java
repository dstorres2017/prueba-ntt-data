package com.nttdata.prueba.models;

import java.util.Date;

import lombok.Data;

@Data
public class RequestMovimiento {

	private int movimientoId;
	private Date fecha ;
	private String tipoMovimiento;
	private double valor;
	private int cuentaId;
	
}
