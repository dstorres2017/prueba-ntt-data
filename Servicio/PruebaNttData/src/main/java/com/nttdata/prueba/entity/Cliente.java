package com.nttdata.prueba.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@lombok.Data
@Entity
@Table(name="tbl_cliente")
@NamedQuery(name="Cliente.findAll",query ="SELECT s FROM Cliente s")
public class Cliente implements Serializable{
	 
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="cliente_id")
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "sq_tbl_cliente_id")
    @SequenceGenerator(name = "sq_tbl_cliente_id",sequenceName = "sq_tbl_cliente_id_db",allocationSize =1)
	private int clienteId;
	
	@JoinColumn(name="persona_id")
	@ManyToOne(cascade = CascadeType.ALL)
	private Persona personaId;
	

	@Column(name="contrasenia")
	private String contrasenia;

	@Column(name="estado")
	private String estado;

	
	@Column(name="usuario_registro")
	private String  usuarioRegistro;
	
	@Column(name="fecha_registro")
	private Date fechaRegistro; 
	
	@PrePersist
	void preInsert() {
		this.fechaRegistro = new Date();
		this.usuarioRegistro = "User_Service";
	}
	

}
