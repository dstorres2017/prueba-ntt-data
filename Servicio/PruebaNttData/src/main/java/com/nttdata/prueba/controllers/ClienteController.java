package com.nttdata.prueba.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.nttdata.prueba.entity.Cliente;
import com.nttdata.prueba.entity.Persona;
import com.nttdata.prueba.models.EntityResponse;
import com.nttdata.prueba.service.ClienteService;
import com.nttdata.prueba.utils.Util;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value="/clientes")
@Api(value = "NttData Service")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2022-08-023T14:50:01.213-05:00")
public class ClienteController {
	
	@Autowired
	private ClienteService ClienteService;

	private HttpStatus code = HttpStatus.INTERNAL_SERVER_ERROR;
	private EntityResponse<?> response = new EntityResponse<>();
	
	private ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();

	@ApiOperation(value = "getall", notes = "listar cliente", response = Cliente.class)
	@GetMapping(value = "/getall", produces = { "application/json" })
	public ResponseEntity<Object>  getUserAll () throws JsonProcessingException {
	
		response = (EntityResponse<?>) ClienteService.consultarTodos();
		code = Util.getCodeHttp(response.getCode());
		return new ResponseEntity<>(response, code);
	}
	
	@GetMapping(value = "/get/{clienteId}", produces = { MediaType.APPLICATION_JSON_VALUE })
	@ApiOperation(value = "getCliente", notes = "Consultar un Cliente", response = EntityResponse.class)
	public ResponseEntity<Object> obtenerCliente(@PathVariable("clienteId") int clienteId) {

		response = ClienteService.consultaCliente(clienteId);
		code = Util.getCodeHttp(response.getCode());
		return new ResponseEntity<>(response, code);
	}
	
	@ApiOperation(value = "insertar", notes = "insertar cliente", response = Cliente.class)
	@PostMapping(value = "/insert", produces = { "application/json" })
	public ResponseEntity<Object>  guardar(@RequestBody  Cliente request) throws JsonProcessingException {
		
		response = (EntityResponse<?>) ClienteService.guardarModificar(requestPostToEntity(request));
		code = Util.getCodeHttp(response.getCode());

		return new ResponseEntity<>(ow.writeValueAsString(response), code);
	}
	
	@PatchMapping(value = "/update", produces = { "application/json" }) //(produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	@ApiOperation(value = "update", notes = "Actualiza Cliente", response = EntityResponse.class)
	public ResponseEntity<Object> actualizarCliente(@RequestBody Cliente requestPatch) throws JsonProcessingException {

		response = (EntityResponse<?>) ClienteService.guardarModificar(requestPatchToEntity(requestPatch));
		code = Util.getCodeHttp(response.getCode());

		return new ResponseEntity<>(ow.writeValueAsString(response), code);
	}
	
	
	@RequestMapping(value = "/delete/{clienteId}",method=RequestMethod.DELETE)
	@ApiOperation(value = "delete", notes = "Eliminar Cliente", response = EntityResponse.class)
	public ResponseEntity<Object> eliminarCliente(@PathVariable("clienteId") int clienteId) throws JsonProcessingException {

		response = (EntityResponse<?>) ClienteService.delete(clienteId);
		code = Util.getCodeHttp(response.getCode());
		return new ResponseEntity<>(ow.writeValueAsString(response), code);
	}

	
	private Cliente requestPostToEntity(Cliente clienteRequest) {
		Cliente cliente = requestToEntity(clienteRequest);
		Persona persona = cliente.getPersonaId();
		cliente.setClienteId(0);
		persona.setPersonaId(0);
		cliente.setPersonaId(persona);
		return cliente;
	}
	
	private Cliente requestPatchToEntity(Cliente request) {
		return requestToEntity(request);
	}
	private Cliente requestToEntity(Cliente clienteRequest) {
		Cliente cliente = new Cliente();
		Persona persona = new Persona();
		cliente.setContrasenia(clienteRequest.getContrasenia());
		cliente.setEstado( clienteRequest.getEstado());
		cliente.setClienteId(clienteRequest.getClienteId());
		
		persona.setDireccion(clienteRequest.getPersonaId().getDireccion());
		persona.setEdad(clienteRequest.getPersonaId().getEdad());
		persona.setGenero(clienteRequest.getPersonaId().getGenero());
		persona.setIdentificacion(clienteRequest.getPersonaId().getIdentificacion());
		persona.setNombre(clienteRequest.getPersonaId().getNombre());
		persona.setPersonaId(clienteRequest.getPersonaId().getPersonaId());
		persona.setTelefono(clienteRequest.getPersonaId().getTelefono());
		
		cliente.setPersonaId(persona);
		return cliente;
	}

}
