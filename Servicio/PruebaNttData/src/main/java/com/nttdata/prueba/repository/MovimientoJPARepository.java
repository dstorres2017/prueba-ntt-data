package com.nttdata.prueba.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.nttdata.prueba.entity.Movimiento;
import com.nttdata.prueba.models.MovimientoCuentaResponse;

public interface MovimientoJPARepository extends  JpaRepository<Movimiento, Integer>{
	
	
	@Query(value= "Select new com.nttdata.prueba.models.MovimientoCuentaResponse( m.fecha , m.cuenta.cliente.personaId.nombre, m.cuenta.numeroCuenta,  m.cuenta.tipoCuenta,  m.cuenta.saldoInicial,  m.cuenta.estado, m.valor, m.saldo) from Movimiento m "
			+ "  where  m.cuenta.numeroCuenta = :numeroCuenta")
	
	List<MovimientoCuentaResponse>  findMovimientoCuenta(@Param("numeroCuenta") String numeroCuenta);

	@Query(value= "Select new com.nttdata.prueba.models.MovimientoCuentaResponse( m.fecha , m.cuenta.cliente.personaId.nombre, m.cuenta.numeroCuenta,  m.cuenta.tipoCuenta,  m.cuenta.saldoInicial,  m.cuenta.estado, m.valor, m.saldo) from Movimiento m "
			+ "  where  m.cuenta.cliente.personaId.identificacion = :identificacion and m.fecha between :fechaInicio and  :fechaFin")
	List<MovimientoCuentaResponse>  findMovimientoCuentaFecha(@Param("identificacion") String identificacion,
			@Param("fechaInicio") Date fechaInicio ,@Param("fechaFin") Date fechaFin);
}
