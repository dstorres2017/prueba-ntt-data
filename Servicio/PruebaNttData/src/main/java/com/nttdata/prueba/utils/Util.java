package com.nttdata.prueba.utils;

import org.springframework.http.HttpStatus;

public class Util {
	//Globales
	public static HttpStatus getCodeHttp(String code) {

		HttpStatus codeHttp;

		switch (code) {
		case "200":
			codeHttp = HttpStatus.OK;
			break;
		case "201":
			codeHttp = HttpStatus.CREATED;
			break;
		case "404":
			codeHttp = HttpStatus.NOT_FOUND;
			break;
		default:
			codeHttp = HttpStatus.INTERNAL_SERVER_ERROR;
			break;
		}

		return codeHttp;
	}
}
