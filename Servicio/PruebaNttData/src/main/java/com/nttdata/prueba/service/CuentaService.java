package com.nttdata.prueba.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nttdata.prueba.entity.Cliente;
import com.nttdata.prueba.entity.Cuenta;
import com.nttdata.prueba.models.EntityResponse;
import com.nttdata.prueba.models.RequestCuenta;
import com.nttdata.prueba.repository.CuentaRepository;
import com.nttdata.prueba.utils.UtilityManager;

import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
@Service
@Transactional
public class CuentaService {
	
	@Autowired
	CuentaRepository cuentaRepository;
	
	@Autowired
	private ClienteService clienteService;
	
	private static final Logger LOGGER = LogManager.getLogger(CuentaService.class);
	private UtilityManager utilityManager = new UtilityManager();
	private EntityResponse<Object> response = new EntityResponse<>();	
	
	private static String statusOK = "OK";
	private static String statusCreated = "CREATED";
	private Cuenta cuenta;
	private Cliente cliente;
	
	
	public Object consultarTodos() {
		LOGGER.info("**** Inicio metodo consulta cuentas ****");

		try {

			response = utilityManager.setEntityResponse(String.valueOf(HttpStatus.OK.value()), statusOK,
					 cuentaRepository.findAll());

		} catch (Exception e) {
			response = utilityManager.setEntityResponse(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()),
					"Consulta de Cuentas fallida",null);
		}

		LOGGER.debug("Response consulta de cuentas: %s", response);

		return response;
	} 
	
	
	@Transactional(rollbackFor = Exception.class)
	public Object guardarModificar(RequestCuenta cuentaRequest) {
		String metodo = cuentaRequest.getCuentaId() == 0 ? "creación" : "modificación";
		LOGGER.info("**** Inicio metodo "+metodo+" Cuenta ****");
		Cuenta obtenerDato ;
		try {
			cliente = (Cliente) clienteService.consultaCliente(cuentaRequest.getClienteId()).getResponse();
			if (cliente == null) {
				response = utilityManager.setEntityResponse(String.valueOf(HttpStatus.BAD_REQUEST.value()),"No existe el cliente ingresado",null);
				return response;
			}
		
			if(metodo.equalsIgnoreCase("modificación")) {
				obtenerDato = requestPatchToEntity (cuentaRequest);
				cuenta = cuentaRepository.findById(cuentaRequest.getCuentaId()).orElse(null);
				if (cuenta == null) {
					response = utilityManager.setEntityResponse(String.valueOf(HttpStatus.BAD_REQUEST.value()),"No existe la cuenta",null);
					return response;
				}
			}else
				obtenerDato = requestPostToEntity (cuentaRequest);
			
			cuenta = cuentaRepository.save(obtenerDato);
			
			response = utilityManager.setEntityResponse(String.valueOf(HttpStatus.CREATED.value()), statusCreated,
					"La "+metodo+" del cuenta fue exitosa");

		} catch (Exception e) {
			response = utilityManager.setEntityResponse(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()),
					e.getMessage(),null);

		} finally {
			LOGGER.debug("Response "+metodo+" de cuenta: %s", response);
		}

		return response;
	
	}
	
	public EntityResponse<Object> consultaCuenta(int cuentaId) {

		LOGGER.info("**** Inicio metodo consulta Cuenta ****");

		try {

			cuenta = cuentaRepository.findByCuentaId(cuentaId);

			response = utilityManager.setEntityResponse(String.valueOf(HttpStatus.OK.value()), statusOK,
					cuenta);

		} catch (Exception e) {
			response = utilityManager.setEntityResponse(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()),
					"Consulta de Cuenta fallida",null);
		}

		LOGGER.debug("Response consulta de Cuenta: %s", response);

		return response;
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Object delete(int cuentaId) {
		
		LOGGER.info("**** Inicio metodo consulta cuenta ****");

		try {

			cuenta = cuentaRepository.findByCuentaId(cuentaId);
			if ( cuenta == null )
				response = utilityManager.setEntityResponse(String.valueOf(HttpStatus.OK.value()), "Cuenta no existe",
						cuenta);
			else {
				cuentaRepository.delete(cuenta);
				response = utilityManager.setEntityResponse(String.valueOf(HttpStatus.OK.value()), "Cuenta eliminada con Exito!!!!",
						cuenta);
			} 
				

		} catch (Exception e) {
			response = utilityManager.setEntityResponse(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()),
					"Consulta de cuenta fallida",null);
		}

		LOGGER.debug("Response consulta de cuenta: %s", response);

		return response;
		
	}
	
	private Cuenta requestPostToEntity(RequestCuenta request) {
		Cuenta cuenta = requestToEntity(request);
		cuenta.setCuentaId(0);
		return cuenta;
	}
	
	private Cuenta requestPatchToEntity(RequestCuenta request) {
		return requestToEntity(request);
	}
	private Cuenta requestToEntity(RequestCuenta request) {
		Cuenta cuenta = new Cuenta();
		cuenta.setCliente(cliente);
		cuenta.setEstado(request.getEstado());
		cuenta.setNumeroCuenta( request.getNumeroCuenta());
		cuenta.setSaldoInicial( request.getSaldoInicial());
		cuenta.setTipoCuenta( request.getTipoCuenta());
		cuenta.setCuentaId( request.getCuentaId());
		cuenta.setUsuarioRegistro("User_Service");
		cuenta.setFechaRegistro( new Date());
		return cuenta;
	}

	
	
}
