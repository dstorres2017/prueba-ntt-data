package com.nttdata.prueba.models;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class MovimientoCuentaResponse {
	
	@JsonProperty("Fecha")
	private Date fecha;
	
	@JsonProperty("Cliente")
	private String nombre;
	
	@JsonProperty("Numero Cuenta")
	private String numeroCuenta;
	
	@JsonProperty("tipo")
	private String tipoCuenta;
	
	@JsonProperty("Saldo Inicial")
	private double saldoInicial;
	
	private String estado;
	
	@JsonProperty("Movimiento")
	private double valor;
	
	@JsonProperty("Saldo Disponible")
	private double saldo;

	public MovimientoCuentaResponse(Date fecha, String nombre, String numeroCuenta, String tipoCuenta,
			double saldoInicial, String estado, double valor, double saldo) {
		this.fecha = fecha;
		this.nombre = nombre;
		this.numeroCuenta = numeroCuenta;
		this.tipoCuenta = tipoCuenta;
		this.saldoInicial = saldoInicial;
		this.estado = estado;
		this.valor = valor;
		this.saldo = saldo;
	}
	
	
}
