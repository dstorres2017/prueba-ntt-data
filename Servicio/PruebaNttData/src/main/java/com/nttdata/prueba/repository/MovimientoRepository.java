package com.nttdata.prueba.repository;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.nttdata.prueba.entity.Movimiento;


@Repository
public interface MovimientoRepository extends CrudRepository<Movimiento, Integer>{
	
	Movimiento findByMovimientoId(int movimientoId);
	
	
}
