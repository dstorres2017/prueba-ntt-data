package com.nttdata.prueba.models;

@lombok.Data
public class RequestCuenta {

	private int cuentaId;
	private String numeroCuenta ;
	private String tipoCuenta;
	private double saldoInicial ;
	private String estado;
	private int clienteId;

}