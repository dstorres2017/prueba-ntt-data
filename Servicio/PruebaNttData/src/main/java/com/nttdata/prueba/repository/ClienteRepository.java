package com.nttdata.prueba.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.nttdata.prueba.entity.Cliente;
@Repository
public interface ClienteRepository extends CrudRepository<Cliente, Integer>{
	
	Cliente findByClienteId(int clienteId);
	
}
