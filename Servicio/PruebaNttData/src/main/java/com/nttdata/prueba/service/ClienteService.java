package com.nttdata.prueba.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nttdata.prueba.entity.Cliente;
import com.nttdata.prueba.models.EntityResponse;
import com.nttdata.prueba.repository.ClienteRepository;
import com.nttdata.prueba.utils.UtilityManager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Service
@Transactional
public class ClienteService {
	
	@Autowired
	ClienteRepository clienteRepository;
	
	private static final Logger LOGGER = LogManager.getLogger(ClienteService.class);
	private UtilityManager utilityManager = new UtilityManager();
	private EntityResponse<Object> response = new EntityResponse<>();	
	
	private static String statusOK = "OK";
	private static String statusCreated = "CREATED";
	private Cliente cliente;
	
	public Object consultarTodos() {
		LOGGER.info("**** Inicio metodo consulta Cliente ****");

		try {

			response = utilityManager.setEntityResponse(String.valueOf(HttpStatus.OK.value()), statusOK,
					 clienteRepository.findAll());

		} catch (Exception e) {
			response = utilityManager.setEntityResponse(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()),
					"Consulta de Clientes fallida",null);
		}

		LOGGER.debug("Response consulta de Cliente: %s", response);

		return response;
	} 
	
	@Transactional(rollbackFor = Exception.class)
	public Object guardarModificar(Cliente clienteRequest) {
		
		

		String metodo = clienteRequest.getClienteId() == 0 ? "creación" : "modificación";
		
		LOGGER.info("**** Inicio metodo "+metodo+" Cliente ****");

		try {

			
			if(metodo.equalsIgnoreCase("modificación")) {
				cliente = clienteRepository.findById(clienteRequest.getClienteId()).orElse(null);
				if (cliente == null) {
					response = utilityManager.setEntityResponse(String.valueOf(HttpStatus.BAD_REQUEST.value()),"No existe el Cliente",null);
					return response;
				}
			}

			cliente = clienteRepository.save(clienteRequest);
			
			response = utilityManager.setEntityResponse(String.valueOf(HttpStatus.CREATED.value()), statusCreated,
					"La "+metodo+" del cliente fue exitosa");

		} catch (Exception e) {
			response = utilityManager.setEntityResponse(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()),
					e.getMessage(),null);

		} finally {
			LOGGER.debug("Response "+metodo+" de Cliente: %s", response);
		}

		return response;
	
	}
	
	public EntityResponse<Object> consultaCliente(int clienteId) {

		LOGGER.info("**** Inicio metodo consulta Cliente ****");

		try {

			cliente = clienteRepository.findByClienteId(clienteId);

			response = utilityManager.setEntityResponse(String.valueOf(HttpStatus.OK.value()), statusOK,
					cliente);

		} catch (Exception e) {
			response = utilityManager.setEntityResponse(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()),
					"Consulta de Cliente fallida",null);
		}

		LOGGER.debug("Response consulta de Cliente: %s", response);

		return response;
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Object delete(int clienteId) {
		
		LOGGER.info("**** Inicio metodo consulta Cliente ****");

		try {

			cliente = clienteRepository.findByClienteId(clienteId);
			if ( cliente == null )
				response = utilityManager.setEntityResponse(String.valueOf(HttpStatus.OK.value()), "Cliente no existe",
					cliente);
			else {
				clienteRepository.delete(cliente);
				response = utilityManager.setEntityResponse(String.valueOf(HttpStatus.OK.value()), "Cliente eliminado con Exito!!!!",
						cliente);
			} 
				

		} catch (Exception e) {
			response = utilityManager.setEntityResponse(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()),
					"Consulta de Cliente fallida",null);
		}

		LOGGER.debug("Response consulta de Cliente: %s", response);

		return response;
		
		/*Cliente cliente = clienteRepository.findById(clienteId).get();
		cliente.setEstado("False");
		return clienteRepository.save(cliente);*/
	}
	
	
}
