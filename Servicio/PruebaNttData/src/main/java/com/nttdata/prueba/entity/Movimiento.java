package com.nttdata.prueba.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@lombok.Data
@Entity
@Table(name="tbl_movimiento")
@NamedQuery(name="Movimiento.findAll",query ="SELECT s FROM Movimiento s")
public class Movimiento implements Serializable{
	 
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="movimiento_id")
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "sq_tbl_movimiento_id")
    @SequenceGenerator(name = "sq_tbl_movimiento_id",sequenceName = "sq_tbl_movimiento_id_db",allocationSize =1)
	private int movimientoId;
	
	@Column(name="fecha")
	private Date fecha ;
	
	@Column(name="tipo_movimiento")
	private String tipoMovimiento;
	
	@Column(name="valor")
	private double valor;
	
	@Column(name="saldo")
	private double saldo;
	
	@JoinColumn(name="Cuenta_id")
	@ManyToOne
	private Cuenta cuenta;
	
	
	@Column(name="usuario_registro")
	private String  usuarioRegistro;
	
	@Column(name="fecha_registro")
	private Date fechaRegistro; 
	
	@PrePersist
	void preInsert() {
		this.fechaRegistro = new Date();
		this.usuarioRegistro = "User_Service";
	}
	
}
