package com.nttdata.prueba.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.nttdata.prueba.entity.Movimiento;
import com.nttdata.prueba.models.EntityResponse;
import com.nttdata.prueba.models.RequestMovimiento;
import com.nttdata.prueba.service.MovimientoService;
import com.nttdata.prueba.utils.Util;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value="/movimientos/")
@Api(value = "NttData Service")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2022-08-023T14:50:01.213-05:00")
public class MovimientoController {
	
	@Autowired
	private MovimientoService movimientosService;
	

	private HttpStatus code = HttpStatus.INTERNAL_SERVER_ERROR;
	private EntityResponse<?> response = new EntityResponse<>();
	
	private ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();

	@ApiOperation(value = "get-all", notes = "listar Movimientos", response = Movimiento.class)
	@GetMapping(value = "/getall", produces = { "application/json" })
	public ResponseEntity<Object>  getMovimientoAll () throws JsonProcessingException {
		
		response = (EntityResponse<?>) movimientosService.consultarTodos();
		code = Util.getCodeHttp(response.getCode());
		return new ResponseEntity<>(response, code);
	}
	
	@GetMapping(value = "/get/{numeroCuenta}", produces = { MediaType.APPLICATION_JSON_VALUE })
	@ApiOperation(value = "getMovimiento", notes = "Consultar una movimiento", response = EntityResponse.class)
	public ResponseEntity<Object> obtenerMovimiento(@PathVariable("numeroCuenta") String numeroCuenta) {

		response = movimientosService.consultaMovimiento(numeroCuenta);
		code = Util.getCodeHttp(response.getCode());
		return new ResponseEntity<>(response, code);
	}
	
	@GetMapping(value = "/getGenerarReporte/{identificacion}/{FechaInicio}/{FechaFin}", produces = { MediaType.APPLICATION_JSON_VALUE })
	@ApiOperation(value = "getGenerarReporte", notes = "Generar Reporte", response = EntityResponse.class)
	public ResponseEntity<Object> generarReporte(@PathVariable("identificacion") String identificacion,
			@PathVariable("FechaInicio") String FechaInicio,
			@PathVariable("FechaFin") String FechaFin) {

		response = movimientosService.consultaReporte(identificacion,FechaInicio,FechaFin);
		code = Util.getCodeHttp(response.getCode());
		return new ResponseEntity<>(response, code);
	}
	
	@ApiOperation(value = "insertar", notes = "insertar movimiento", response = Movimiento.class)
	@PostMapping(value = "/insert", produces = { "application/json" })
	public ResponseEntity<Object>  guardar(@RequestBody  RequestMovimiento Request) throws JsonProcessingException {
		
		response = (EntityResponse<?>) movimientosService.guardarModificar(Request);
		code = Util.getCodeHttp(response.getCode());

		return new ResponseEntity<>(ow.writeValueAsString(response), code);
	}
	
	@PatchMapping(value = "/update", produces = { "application/json" })
	@ApiOperation(value = "update", notes = "Actualiza movimiento", response = EntityResponse.class)
	public ResponseEntity<Object> actualizarCuenta(@RequestBody RequestMovimiento Request) throws JsonProcessingException {

		response = (EntityResponse<?>) movimientosService.guardarModificar(Request);
		code = Util.getCodeHttp(response.getCode());

		return new ResponseEntity<>(ow.writeValueAsString(response), code);
	}
	
	
	@RequestMapping(value = "/delete/{movimientoId}",method=RequestMethod.DELETE)
	@ApiOperation(value = "delete", notes = "Eliminar Movimiento", response = EntityResponse.class)
	public ResponseEntity<Object> eliminarMovimiento(@PathVariable("movimientoId") int movimientoId) throws JsonProcessingException {

		response = (EntityResponse<?>) movimientosService.delete(movimientoId);
		code = Util.getCodeHttp(response.getCode());
		return new ResponseEntity<>(ow.writeValueAsString(response), code);
	}


}
