package com.nttdata.prueba.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@lombok.Data
@Entity
@Table(name="tbl_persona")
@NamedQuery(name="Persona.findAll",query ="SELECT s FROM Persona s")
public class Persona implements Serializable{
	 
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="persona_id")
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "sq_tbl_persona_id")
    @SequenceGenerator(name = "sq_tbl_persona_id",sequenceName = "sq_tbl_persona_id_db",allocationSize =1)
	private int personaId;
	
	@Column(name="nombre")
	private String nombre ;
	
	@Column(name="genero")
	private String genero;
	
	@Column(name="edad")
	private int edad ;
	
	@Column(name="identificacion")
	private String identificacion;
	
	@Column(name="direccion")
	private String direccion;
	
	@Column(name="telefono")
	private String telefono;
	
	@Column(name="usuario_registro")
	private String  usuarioRegistro;
	
	@Column(name="fecha_registro")
	private Date fechaRegistro; 
	
	@PrePersist
	void preInsert() {
		this.fechaRegistro = new Date();
		this.usuarioRegistro = "User_Service";
	}

}
