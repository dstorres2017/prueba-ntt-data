package com.nttdata.prueba.models;

import lombok.Data;

@Data
public class ResponseGeneral {

	private String code;
	private String message;
	
}
