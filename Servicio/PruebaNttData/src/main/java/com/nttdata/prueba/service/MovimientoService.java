package com.nttdata.prueba.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nttdata.prueba.entity.Cuenta;
import com.nttdata.prueba.entity.Movimiento;
import com.nttdata.prueba.models.EntityResponse;
import com.nttdata.prueba.models.MovimientoCuentaResponse;
import com.nttdata.prueba.models.RequestMovimiento;
import com.nttdata.prueba.repository.MovimientoJPARepository;
import com.nttdata.prueba.repository.MovimientoRepository;
import com.nttdata.prueba.utils.UtilityManager;

import java.text.SimpleDateFormat;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Service
@Transactional
public class MovimientoService {
	
	@Autowired
	MovimientoRepository movimientoRepository;
	
	@Autowired
	MovimientoJPARepository movimientoJPARepository;
	
	@Autowired
	private CuentaService cuentaService; 

	
	private static final Logger LOGGER = LogManager.getLogger(MovimientoService.class);
	private UtilityManager utilityManager = new UtilityManager();
	private EntityResponse<Object> response = new EntityResponse<>();	
	
	private static String statusOK = "OK";
	private static String statusCreated = "CREATED";
	private Movimiento movimiento;
	private Cuenta cuenta;
	
	public Object consultarTodos() {
		LOGGER.info("**** Inicio metodo consulta movimientos ****");

		try {

			response = utilityManager.setEntityResponse(String.valueOf(HttpStatus.OK.value()), statusOK,
					movimientoRepository.findAll());

		} catch (Exception e) {
			response = utilityManager.setEntityResponse(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()),
					"Consulta de Cuentas fallida",null);
		}

		LOGGER.debug("Response consulta de movimientos: %s", response);

		return response;
	} 
	
	@Transactional(rollbackFor = Exception.class)
	public Object guardarModificar(RequestMovimiento movimientoRequest) {
		String metodo = movimientoRequest.getMovimientoId() == 0 ? "creación" : "modificación";
		LOGGER.info("**** Inicio metodo "+metodo+" movimiento ****");
		Movimiento obtenerDato ;
		
		try {
			
			cuenta = (Cuenta) cuentaService.consultaCuenta(movimientoRequest.getCuentaId()).getResponse();
			if (cuenta == null) {
				response = utilityManager.setEntityResponse(String.valueOf(HttpStatus.BAD_REQUEST.value()),"No existe la cuenta ingresada",null);
				return response;
			}
			
			if ((cuenta.getSaldoInicial()  + movimientoRequest.getValor() )  <= 0  && movimientoRequest.getTipoMovimiento().equalsIgnoreCase("Debito")) {
				response = utilityManager.setEntityResponse(String.valueOf(HttpStatus.BAD_REQUEST.value()),"Saldo no disponible",null);
				return response;
			}
			
			
			if(metodo.equalsIgnoreCase("modificación")) {
				obtenerDato = requestPatchToEntity (movimientoRequest);
				movimiento = movimientoRepository.findById(movimientoRequest.getMovimientoId()).orElse(null);
				if (movimiento == null) {
					response = utilityManager.setEntityResponse(String.valueOf(HttpStatus.BAD_REQUEST.value()),"No existe la movimiento",null);
					return response;
				}
			}else
				obtenerDato = requestPostToEntity (movimientoRequest);

			movimiento = movimientoRepository.save(obtenerDato);
			
			response = utilityManager.setEntityResponse(String.valueOf(HttpStatus.CREATED.value()), statusCreated,
					"La "+metodo+" del movimiento fue exitosa");

		} catch (Exception e) {
			response = utilityManager.setEntityResponse(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()),
					e.getMessage(),null);

		} finally {
			LOGGER.debug("Response "+metodo+" de movimiento: %s", response);
		}

		return response;
	
	}
	
	public EntityResponse<Object> consultaMovimiento(String numeroCuenta) {

		LOGGER.info("**** Inicio metodo consulta movimiento ****");

		try {
			List<MovimientoCuentaResponse> movimientoCuentaResponse = movimientoJPARepository.findMovimientoCuenta(numeroCuenta);
			response = utilityManager.setEntityResponse(String.valueOf(HttpStatus.OK.value()), statusOK,
					movimientoCuentaResponse);

		} catch (Exception e) {
			response = utilityManager.setEntityResponse(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()),
					"Consulta de movimiento fallida",null);
		}

		LOGGER.debug("Response consulta de movimiento: %s", response);

		return response;
	}
	
	
	public EntityResponse<Object> consultaReporte(String identificacion, String fechaInicio, String fechaFin) {

		LOGGER.info("**** Inicio metodo  consultaReporte ****");
		SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy"); 
		try {
			List<MovimientoCuentaResponse> movimientoCuentaResponse = movimientoJPARepository.findMovimientoCuentaFecha(identificacion, formato.parse(fechaInicio) ,formato.parse(fechaFin)  );
			response = utilityManager.setEntityResponse(String.valueOf(HttpStatus.OK.value()), statusOK,
					movimientoCuentaResponse);

		} catch (Exception e) {
			response = utilityManager.setEntityResponse(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()),
					"Consulta de movimiento fallida",null);
		}

		LOGGER.debug("Response consulta de movimiento: %s", response);

		return response;
	}
	
	@Transactional(rollbackFor = Exception.class)
	public Object delete(int movimientoId) {
		
		LOGGER.info("**** Inicio metodo consulta movimiento ****");

		try {

			movimiento = movimientoRepository.findByMovimientoId(movimientoId);
			if ( movimiento == null )
				response = utilityManager.setEntityResponse(String.valueOf(HttpStatus.OK.value()), "movimiento no existe",
						movimiento);
			else {
				movimientoRepository.delete(movimiento);
				response = utilityManager.setEntityResponse(String.valueOf(HttpStatus.OK.value()), "movimiento eliminada con Exito!!!!",
						movimiento);
			} 
				

		} catch (Exception e) {
			response = utilityManager.setEntityResponse(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()),
					"Consulta de movimiento fallida",null);
		}

		LOGGER.debug("Response consulta de movimiento: %s", response);

		return response;
		
	}
	
	private Movimiento requestPostToEntity(RequestMovimiento request) {
		Movimiento movimeinto = requestToEntity(request);
		movimeinto.setMovimientoId(0);
		return movimeinto;
	}
	
	private Movimiento requestPatchToEntity(RequestMovimiento request) {
		return requestToEntity(request);
	}
	private Movimiento requestToEntity(RequestMovimiento request) {
		Movimiento movimiento = new Movimiento();
		movimiento.setCuenta(cuenta);
		movimiento.setFecha(request.getFecha());
		movimiento.setSaldo(cuenta.getSaldoInicial()+request.getValor());
		movimiento.setTipoMovimiento( request.getTipoMovimiento());
		movimiento.setValor( request.getValor());
		
		return movimiento;
	}
	
	
}
