package com.nttdata.prueba.models;

import lombok.Data;

@Data
public class EntityResponse<T> {
	private String status;
	private String code;
	private T response;
}
