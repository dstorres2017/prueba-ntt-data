package com.nttdata.prueba.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.nttdata.prueba.entity.Cuenta;
import com.nttdata.prueba.models.EntityResponse;
import com.nttdata.prueba.models.RequestCuenta;
import com.nttdata.prueba.service.CuentaService;
import com.nttdata.prueba.utils.Util;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value="/cuentas/")
@Api(value = "NttData Service")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2022-08-023T14:50:01.213-05:00")
public class CuentaController {
	
	@Autowired
	private CuentaService cuentaService;

	private HttpStatus code = HttpStatus.INTERNAL_SERVER_ERROR;
	private EntityResponse<?> response = new EntityResponse<>();
	
	private ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();

	@ApiOperation(value = "get-all", notes = "listar Cuentas", response = Cuenta.class)
	@GetMapping(value = "/getall", produces = { "application/json" })
	public ResponseEntity<Object>  getCuentaAll () throws JsonProcessingException {
		
		response = (EntityResponse<?>) cuentaService.consultarTodos();
		code = Util.getCodeHttp(response.getCode());
		return new ResponseEntity<>(response, code);
	}
	
	@GetMapping(value = "/get/{cuentaId}", produces = { MediaType.APPLICATION_JSON_VALUE })
	@ApiOperation(value = "getCuenta", notes = "Consultar una cuenta", response = EntityResponse.class)
	public ResponseEntity<Object> obtenerCuenta(@PathVariable("cuentaId") int cuentaId) {

		response = cuentaService.consultaCuenta(cuentaId);
		code = Util.getCodeHttp(response.getCode());
		return new ResponseEntity<>(response, code);
	}
	
	@ApiOperation(value = "insertar", notes = "insertar cuenta", response = Cuenta.class)
	@PostMapping(value = "/insert", produces = { "application/json" })
	public ResponseEntity<Object>  guardar(@RequestBody  RequestCuenta Request) throws JsonProcessingException {
		
		response = (EntityResponse<?>) cuentaService.guardarModificar(Request);
		code = Util.getCodeHttp(response.getCode());

		return new ResponseEntity<>(ow.writeValueAsString(response), code);
	}
	
	@PatchMapping(value = "/update", produces = { "application/json" })//(produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	@ApiOperation(value = "update", notes = "Actualiza cuenta", response = EntityResponse.class)
	public ResponseEntity<Object> actualizarCuenta(@RequestBody RequestCuenta requestPatch) throws JsonProcessingException {
		response = (EntityResponse<?>) cuentaService.guardarModificar(requestPatch);
		code = Util.getCodeHttp(response.getCode());

		return new ResponseEntity<>(ow.writeValueAsString(response), code);
	}
	
	
	@RequestMapping(value = "/delete/{cuentaId}",method=RequestMethod.DELETE)
	@ApiOperation(value = "delete", notes = "Eliminar Cuenta", response = EntityResponse.class)
	public ResponseEntity<Object> eliminarCuenta(@PathVariable("cuentaId") int cuentaId) throws JsonProcessingException {

		response = (EntityResponse<?>) cuentaService.delete(cuentaId);
		code = Util.getCodeHttp(response.getCode());
		return new ResponseEntity<>(ow.writeValueAsString(response), code);
	}

}
