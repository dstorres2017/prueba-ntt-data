package com.nttdata.prueba.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.nttdata.prueba.entity.Cuenta;

@Repository
public interface CuentaRepository extends CrudRepository<Cuenta, Integer>{
	
	Cuenta findByCuentaId(int cuentaId);
	
}
