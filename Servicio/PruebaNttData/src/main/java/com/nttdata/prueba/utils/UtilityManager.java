package com.nttdata.prueba.utils;

import com.nttdata.prueba.models.EntityResponse;
import com.nttdata.prueba.models.ResponseGeneral;

public class UtilityManager {

	public ResponseGeneral setResponse(String code, String message,ResponseGeneral resp) {

		resp.setCode(code);
		resp.setMessage(message);
		return resp;

	}
	
	public EntityResponse<Object> setEntityResponse(String code, String status, Object response) {
		EntityResponse<Object> resp = new EntityResponse<>();
		resp.setCode(code);
		resp.setResponse(response);
		resp.setStatus(status);
		return resp;

	}

}
