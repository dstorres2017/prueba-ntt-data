package com.nttdata.prueba;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class PruebaNttDataApplication {

	public static void main(String[] args) {
		SpringApplication.run(PruebaNttDataApplication.class, args);
	}
	
   @Bean
   RestTemplate restTemplate() {
        return new RestTemplate();
    }
	 
}
