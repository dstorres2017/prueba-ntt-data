package com.nttdata.prueba.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@lombok.Data
@Entity
@Table(name="tbl_cuenta")
@NamedQuery(name="Cuenta.findAll",query ="SELECT s FROM Cuenta s")
public class Cuenta implements Serializable{
	 
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="cuenta_id")
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "sq_tbl_cuenta_id")
   @SequenceGenerator(name = "sq_tbl_cuenta_id",sequenceName = "sq_tbl_cuenta_id_db",allocationSize =1)
	private int cuentaId;
	
	@Column(name="numero_cuenta")
	private String numeroCuenta ;
	
	@Column(name="tipo_cuenta")
	private String tipoCuenta;
	
	@Column(name="saldo_inicial")
	private double saldoInicial ;
	
	@Column(name="estado")
	private String estado;
	
	@JoinColumn(name="cliente_id")
	@ManyToOne
	private Cliente cliente;
	
	@Column(name="usuario_registro")
	private String  usuarioRegistro;
	
	@Column(name="fecha_registro")
	private Date fechaRegistro; 
	
	@PrePersist
	void preInsert() {
		this.fechaRegistro = new Date();
		this.usuarioRegistro = "User_Service";
	}

}
