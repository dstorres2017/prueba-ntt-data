package com.nttdata.prueba;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nttdata.prueba.controllers.CuentaController;
import com.nttdata.prueba.service.CuentaService;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;



@WebMvcTest(CuentaController.class)
class CuentaControllerTests {

    @Autowired
    private MockMvc mockMvc;
 
    @MockBean
    CuentaService movimeintoService;
    
    @Autowired
    private ObjectMapper objectMapper;
    
    @Test
    void testGetSpain() throws Exception {
    	
        /*String response = mockMvc.perform(get("/api/cuentas/getall"))
                .andExpect(status().is(HttpStatus.OK.value()))
                .andExpect(jsonPath("$.status", is("OK")))
                .andReturn().getResponse()
                .getContentAsString();*/
        
        mockMvc.perform(post("/api/cuentas/insert")
        	    .contentType("application/json")
        	    .content(objectMapper.writeValueAsString("{\n"
        	    		+ "  \"clienteId\": 2,\n"
        	    		+ "  \"cuentaId\": 0,\n"
        	    		+ "  \"estado\": \"True\",\n"
        	    		+ "  \"numeroCuenta\": \"01234567899\",\n"
        	    		+ "  \"saldoInicial\": 1000,\n"
        	    		+ "  \"tipoCuenta\": \"Ahorro\"\n"
        	    		+ "}")))
        	    //.andExpect(status().isOk());
        		.andExpect(status().isNotFound());
 
    }
}
