package com.nttdata.prueba;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nttdata.prueba.controllers.MovimientoController;
import com.nttdata.prueba.service.MovimientoService;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;



@WebMvcTest(MovimientoController.class)
class MovimientoControllerTests {

    @Autowired
    private MockMvc mockMvc;
 
    @MockBean
    MovimientoService movimientoService;
    
    @Autowired
    private ObjectMapper objectMapper;
    
    @Test
    void testGetSpain() throws Exception {
    	
        /*String response = mockMvc.perform(get("/api/movimientos/getall"))
                .andExpect(status().is(HttpStatus.OK.value()))
                .andExpect(jsonPath("$.status", is("OK")))
                .andReturn().getResponse()
                .getContentAsString();*/
        
        mockMvc.perform(post("/api/movimientos/insert")
        	    .contentType("application/json")
        	    .content(objectMapper.writeValueAsString("{\n"
        	    		+ "  \"cuentaId\": 21,\n"
        	    		+ "  \"fecha\": \"2022-08-24T05:43:59.650Z\",\n"
        	    		+ "  \"movimientoId\": 0,\n"
        	    		+ "  \"tipoMovimiento\": \"Debito\",\n"
        	    		+ "  \"valor\": -1200\n"
        	    		+ "}")))
        	    //.andExpect(status().isOk());
        		.andExpect(status().isNotFound());
 
    }
}
