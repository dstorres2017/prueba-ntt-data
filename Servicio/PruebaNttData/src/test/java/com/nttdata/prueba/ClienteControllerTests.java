package com.nttdata.prueba;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nttdata.prueba.controllers.ClienteController;
import com.nttdata.prueba.service.ClienteService;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;



@WebMvcTest(ClienteController.class)
class ClienteControllerTests {

    @Autowired
    private MockMvc mockMvc;
 
    @MockBean
    ClienteService clienteService;
    
    @Autowired
    private ObjectMapper objectMapper;
    
    @Test
    void testGetSpain() throws Exception {
    	
        /*String response = mockMvc.perform(get("/api/clientes/getall"))
                .andExpect(status().is(HttpStatus.OK.value()))
                .andExpect(jsonPath("$.status", is("OK")))
                .andReturn().getResponse()
                .getContentAsString();*/
        
        mockMvc.perform(post("/api/clientes/insert")
        	    .contentType("application/json")
        	    .content(objectMapper.writeValueAsString("{\n"
        	    		+ "  \"clienteId\": 0,\n"
        	    		+ "  \"contrasenia\": \"pruebas1234\",\n"
        	    		+ "  \"estado\": \"True\",\n"
        	    		+ "  \"fechaRegistro\": \"2022-08-24T04:45:26.317Z\",\n"
        	    		+ "  \"personaId\": {\n"
        	    		+ "    \"direccion\": \"PRuebas de cliente\",\n"
        	    		+ "    \"edad\": 30,\n"
        	    		+ "    \"fechaRegistro\": \"2022-08-24T04:45:26.317Z\",\n"
        	    		+ "    \"genero\": \"Masculino\",\n"
        	    		+ "    \"identificacion\": \"0912385458\",\n"
        	    		+ "    \"nombre\": \"Daniel Torres\",\n"
        	    		+ "    \"personaId\": 0,\n"
        	    		+ "    \"telefono\": \"0992194333\",\n"
        	    		+ "    \"usuarioRegistro\": \"string\"\n"
        	    		+ "  },\n"
        	    		+ "  \"usuarioRegistro\": \"string\"\n"
        	    		+ "}")))
        	    //.andExpect(status().isOk());
        		.andExpect(status().isNotFound());
 
    }
}
